<?php 
	/**
	 * string	$args['title']
	 * string	$args['content']
	 */
	global $args;
	$card_class = "";
	if( $args['use_card'] ){
		$card_class = "shadow-card";
	}
?>
<div class="<?php echo $card_class; ?> interior-container slower fadeIn animated" >
	<!--
	<div class="orange-fade-vertical left-bar"></div>
	<div class="orange-fade-horizontal top-bar"></div>
-->
	<div class="gc-wrap ">
		<div class="gc-content">
<!--
			<h2>NOVEMBER 29TH - DECEMBER 8TH, 7AM-9PM</h2>
			<i style="font-size:30px;">10 DAYS OF <B>FREE CAR WASHES</B></i>
			<p style="margin-top:20px;">To celebrate the Grand Opening of U-DRIVE Express Car Wash in Las Vegas, we are giving away free Extreme Lava Falls car washes (Our Best Wash).</p>
-->
			<?php echo $args['content']; ?>
		</div>
	</div>
</div>