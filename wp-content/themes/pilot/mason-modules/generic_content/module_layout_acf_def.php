<?php
	global $pilot;
	// add module layout to flexible content 
	$module_layout = array (
		'key' => '569d9ff728231',
		'name' => 'generic_content_block',
		'label' => 'Generic Content Theme',
		'display' => 'block',
		'sub_fields' => array (
		    array(
		        'key' => create_key('generic_content','use_card'),
		        'label' => 'Use Background Card',
		        'name' => 'generic_content_block_use_card',
		        'type' => 'true_false',
		        'instructions' => '',
		        'required' => 0,
		        'wrapper' => array(
		            'width' => '100',
		            'class' =>'' ,
		            'id' => '',
		        ),
		        'message' => '',
		        'default_value' => 0,
		        'ui' => 1,
		        'ui_on_text' => 'Card',
		        'ui_off_text' => 'No Card',
		    ),			
			array (
				'key' => 'field_569da00928232',
				'label' => 'Content',
				'name' => 'generic_content_block_content',
				'type' => 'wysiwyg',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'tabs' => 'all',
				'toolbar' => 'full',
				'media_upload' => 1,
			),
		),
		'min' => '',
		'max' => '',
	);
?>