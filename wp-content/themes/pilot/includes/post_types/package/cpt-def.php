<?php
/**
 * Package Custom Post Type
 */
add_action( 'init', 'register_package_post_type');
function register_package_post_type()
{ 
	register_post_type( 'package',
		array( 'labels' => 
			array(
				'name'               => 'Packages',
				'singular_name'      => 'Package',
				'all_items'          => 'All Packages',
				'add_new'            => 'Add New',
				'add_new_item'       => 'Add New Package',
				'edit'               => 'Edit',
				'edit_item'          => 'Edit Package',
				'new_item'           => 'New Package',
				'view_item'          => 'View Package',
				'search_items'       => 'Search Packages',
				'not_found'          => 'Nothing found in the Database.',
				'not_found_in_trash' => 'Nothing found in Trash',
				'parent_item_colon'  => ''
			),
			'description'         => 'Packages post type',
			'public'              => true,
			'publicly_queryable'  => true,
			'exclude_from_search' => false,
			'show_ui'             => true,
			'query_var'           => true,
			'menu_position'       => 10,
			'menu_icon'           => 'dashicons-store',
			 'rewrite'	      => array( 'slug' => 'package', 'with_front' => false ),
			 'has_archive'      => false,
			'capability_type'     => 'page',
			'supports'            => array( 'title','author')
		)
	);
}
?>