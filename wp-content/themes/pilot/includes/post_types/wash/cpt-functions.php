<?php
	require get_template_directory() . '/includes/post_types/wash/cpt-def.php';
	require get_template_directory() . '/includes/post_types/wash/cpt-acf.php';
	require get_template_directory() . '/includes/post_types/wash/wash.class.php';


function get_all_washs(){
	$washes = array();
	$args=array(
	  'post_type' => 'wash',
	  'post_status' => 'publish',
	  'posts_per_page' => -1
	);
	$pack_query = new WP_Query($args);
	if( $pack_query->have_posts() ):
		foreach($pack_query->posts as $pack_post):
			$washs[] = new wash($pack_post);			
		endforeach;
	endif;
	return $washes;
}
function qcc_wash( $wash_ref = null ){
	$wash = new QCC\wash( $wash_ref );
	if( $wash->is_admin() ){
		return new QCC\Admin( $wash );
	}
	return $wash;

}