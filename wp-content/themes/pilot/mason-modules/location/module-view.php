<?php 
	/**
	 * string	$args['title']
	 * string	$args['content']
	 */
	global $args;
	$args['content'] = "6451 W. Charleston Blvd. Las Vegas, NV 89146";
?>
<div class="shadow-card interior-container slower fadeIn animated">
    <div id="map"></div>
	<div class="loc-content">
		<div class="img-wrapper" style="display:inline-block;  position: relative;
  top: 30%;
  transform: translateY(-95%); margin-right:15px;">
			<img src="<?php echo get_template_directory_uri()."/mason-modules/location/map-pin.svg" ?>">
		</div>
		<div style="display:inline-block">
			<h6>Our Location</h6>
			<div class="address"><?php echo $args['content']; ?></div>
			<div>
			<a target="_blank" href="https://www.google.com/maps/place/6451+W+Charleston+Blvd,+Las+Vegas,+NV+89146/@36.1571001,-115.2392343,15.23z/data=!4m5!3m4!1s0x80c8c11d5f7e8cb3:0x661341aefb09ffa4!8m2!3d36.1586086!4d-115.2361787">Directions</a>
			</div>
		</div>
		<?php if( $args['button']  ) : ?>
			<div class="button-wrapper">
				<a href="<?php echo $args['button']['url']; ?>" class="udrive-button orange-fade-horizontal ">
					<h5><?php echo $args['button']['title']; ?></h5>
				</a>
			</div>
		<?php endif; ?>
	</div>
</div>
    <script>
      var map;
      function initMap() {
      	
		var carwash =  {lat: 36.1586086, lng: -115.2361787};
        map = new google.maps.Map(document.getElementById('map'), {
          center:carwash,
          zoom: 14,
          disableDefaultUI: true
        });
        var icon = '/wp-content/themes/pilot/mason-modules/location/map-pin.svg';
		var marker = new google.maps.Marker({position: carwash, map: map, icon:icon});
      }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBFDxx-tuMiKdNLuQeNLerrfugg1FbjWEw&callback=initMap"
    async defer></script>
