<?php
class QCCPackage{
	public function __construct( $package_ref = null ){
		global $pilot;
		if( is_numeric( $package_ref ) ){
			$package = get_user_by( 'ID', $package_ref );
		}
		elseif( is_object( $package_ref ) ){
			$package = $package_ref;
		}
		if( is_object( $package ) ){
			foreach( $package as $key => $value ){
				$this->$key = $value;
			}
		}
		$this->get_package_meta();
		$this->wp_package = $package;
	}
	public function get_package_meta(){
		$cpt = "package";
		$this->active = get_field($cpt.'_active',$this->ID);
		$this->image = get_field($cpt.'_image',$this->ID);
		$this->name = get_field($cpt.'_name',$this->ID);
		$this->summary = get_field($cpt.'_summary',$this->ID);
		$this->features = get_field($cpt.'_features',$this->ID);
		$this->prices = get_field($cpt.'_prices',$this->ID);
		$this->price_description = get_field($cpt.'_price_description',$this->ID);
		$this->other_info = get_field($cpt.'_other_info',$this->ID);
		$this->link = get_field($cpt.'_link',$this->ID);
	}
}