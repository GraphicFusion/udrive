<?php
/**
 * Template Name: Packages
 *
 */
$args = array('post_type' => 'package');
$packages = new WP_Query($args);
get_header(); ?>
	<?php if ( $packages->have_posts() ) : ?>

		<header class="page-header">
		</header>
<?php get_template_part( 'views/content', 'page' ); ?>

<div class='block-icon module wash-list' id=''>
	<div class='layout-content'>
		<div class="shadow-card interior-container">
		<?php while ( $packages->have_posts() ) : $packages->the_post(); $package = new QCCPackage($post); ?>
			<?php if($package->active) : ?>
				<?php
					$image = "";
					if($package->image){
						$image = '<div class="img-wrapper"><img class="content-img" src="'.$package->image['url'].'"></div>';
					}
				?>
				<div class="icon-wrap wow animated fadeInUp">
					<?php echo $image; ?>
					<div class="icon-content ">
						<div class="title wow fadeInRight animated" data-wow-duration="2s">
							<?php if( $package->name ) : ?>
								<h3><?php echo $package->name; ?></h3>
							<?php endif; ?>
							<?php if( $package->summary ) : ?>
								<h6><?php echo $package->summary ?></h6>
							<?php endif; ?>
							<?php if( $package->features ) : ?>
								<ul>
									<?php foreach( $package->features as $feat) : ?>
										<li><?php echo $feat['package_feature']; ?></li>
									<?php endforeach; ?>
								</ul>
							<?php endif; ?>
							<?php if( $package->prices ) : ?>
								<div class="price-wrapper">
									<?php foreach( $package->prices as $price_arr) : ?>
										<div>
											<div class="price"><?php echo $price_arr['package_price']; ?></div>  -  <div class="price-description"><?php echo $price_arr['package_price_description']; ?></div>
										</div>
									<?php endforeach; ?>
								</div>
							<?php endif; ?>
							<a href="<?php echo $package->link['url']; ?>" class="udrive-button orange-fade-horizontal ">
								<h5>VIew</h5>
							</a>

						</div><!--/title-->
					</div><!--/icon-content-->
				</div><!--/icon-wrap-->
			<?php endif; ?>
		<?php endwhile; ?>
		</div>
	</div>
</div>
		<?php the_posts_navigation(); ?>

	<?php else : ?>

		<?php get_template_part( 'views/content', 'none' ); ?>

	<?php endif; ?>

<?php get_footer(); ?>