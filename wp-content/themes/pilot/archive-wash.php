<?php get_header(); ?>
	<?php if ( have_posts() ) : ?>

		<header class="page-header">
			<?php
				the_archive_title( '<h1 class="page-title">', '</h1>' );
				the_archive_description( '<div class="taxonomy-description">', '</div>' );
			?>
		</header>
<div class='block-icon module' id=''>
	<div class='layout-content'>
		<div class="shadow-card interior-container">
		<?php while ( have_posts() ) : the_post(); $wash = new QCCWash($post); ?>
			<?php if($wash->active) : ?>
				<?php
					$image = "";
					if($wash->image){
						$image = '<div class="img-wrapper"><img class="content-img" src="'.$wash->image['url'].'"></div>';
					}
				?>
				<div class="icon-wrap wow animated fadeInUp">
					<?php echo $image; ?>
					<div class="icon-content ">
						<div class="title wow fadeInRight animated" data-wow-duration="2s">
							<?php if( $wash->name ) : ?>
								<h3><?php echo $wash->name; ?><span class="h4"> Wash</span></h3>
							<?php endif; ?>
							<?php if( $wash->summary ) : ?>
								<h6><?php echo $wash->summary ?></h6>
							<?php endif; ?>
							<?php if( $wash->features ) : ?>
								<ul>
									<?php foreach( $wash->features as $feat) : ?>
										<li><?php echo $feat['wash_feature']; ?></li>
									<?php endforeach; ?>
								</ul>
							<?php endif; ?>
							<?php if( $wash->prices ) : ?>
								<div class="price-wrapper">
									<?php foreach( $wash->prices as $price_arr) : ?>
										<div>
											<div class="price"><?php echo $price_arr['wash_price']; ?></div>  -  <div class="price-description"><?php echo $price_arr['wash_price_description']; ?></div>
										</div>
									<?php endforeach; ?>
								</div>
							<?php endif; ?>
						</div><!--/title-->
					</div><!--/icon-content-->
				</div><!--/icon-wrap-->
			<?php endif; ?>
		<?php endwhile; ?>
		</div>
	</div>
</div>
		<?php the_posts_navigation(); ?>

	<?php else : ?>

		<?php get_template_part( 'views/content', 'none' ); ?>

	<?php endif; ?>

<?php get_footer(); ?>