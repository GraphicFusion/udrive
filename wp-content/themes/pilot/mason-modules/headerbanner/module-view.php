<?php
	global $args;
	global $wp;
	$current_url = home_url( add_query_arg( array(), $wp->request ) );

?>
<?php if( isset($args['parent_title'])):?>
	<div class="parent-header">
		<div class="container content-container interior-container" >
			<div class="title">
				<h6><a href="<?php echo $args['parent_permalink']; ?>"><?php echo $args['parent_title']; ?></a> > <a class="active" href="<?php echo $args['permalink']; ?>"><?php echo $args['title']; ?></a></h6>
				<h1><?php echo $args['title']; ?></h1>
			</div><!--/title-->
		</div><!--/container--> 			
	</div>
	<script>
		$( document ).ready(function() {
			$('body').addClass('has-parent-header');
		});
	</script>
<?php else: ?>
	<div class="img-block-wrap" id="<?php echo $args['id']; ?>">
		<div class="bg-image" style="background-image: url(<?php echo $args['image_url']; ?>);">
			<div style="width:100%; display:flex; position:absolute; bottom:0;">
				<div class="container content-container interior-container dark-card">
					<div class="title ">
						<?php if( isset( $args['title'] ) ) : ?>
							<div class=" media_title">
								<?php echo $args['title']; ?>
							</div><!--/text_effect-->
						<?php endif; ?>
					</div><!--/title-->
	        	</div><!--/container--> 			
	        </div>
		</div><!--/bg-img-->
	</div><!--img-block-wrap-->
<?php endif; ?>