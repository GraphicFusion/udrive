<?php
	require get_template_directory() . '/includes/post_types/package/cpt-def.php';
	require get_template_directory() . '/includes/post_types/package/cpt-acf.php';
	require get_template_directory() . '/includes/post_types/package/package.class.php';


function get_all_packages(){
	$packages = array();
	$args=array(
	  'post_type' => 'package',
	  'post_status' => 'publish',
	  'posts_per_page' => -1
	);
	$pack_query = new WP_Query($args);
	if( $pack_query->have_posts() ):
		foreach($pack_query->posts as $pack_post):
			$packages[] = new package($pack_post);			
		endforeach;
	endif;
	return $packages;
}
function qcc_package( $package_ref = null ){
	$package = new QCC\package( $package_ref );
	if( $package->is_admin() ){
		return new QCC\Admin( $package );
	}
	return $package;

}