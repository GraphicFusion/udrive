<?php
	global $args;
	$block_class = "";
	$suffixes = [""];
	if($args['half_width'] ){
		$block_class = "half-width";
		$suffixes[] = "_second";
	}
?>
<div class="interior-container slower fadeIn animated" >
<?php
	foreach($suffixes as $suffix){
		$use_popup = 0;
		if( $args['use_popup'.$suffix] ){
			$use_popup = 1;
			$popup_video = $args['video_file_mp4'.$suffix];
			if( $args['youtube'.$suffix] ) {
				$popup_video = $args['youtube'.$suffix];
			}
		}
		$use_banner = 0;
		if( $args['use_banner'.$suffix] && ($args['video_file_mp4'.$suffix] || $args['youtube'.$suffix] || $args['bg_image_url'.$suffix] ) ){
			$use_banner = 1;
		}
		$use_bg_image = 0;
		$use_bg_video = 1;
		if($args['use_image'.$suffix]){
			$use_bg_image = 1;
			$use_bg_video = 0;		
		}
		$use_overlay = 0;
		if( $args['use_overlay'.$suffix] ){
			$use_overlay = 1;
		}
	?>
		<div class="shadow-card <?php echo $block_class." ".$suffix; ?>">
			<?php if( $use_banner ) : ?>
				<div class="img-block-wrap" id="<?php echo $args['id'.$suffix]; ?>">
					<div class="bg-image" <?php if( $use_bg_image){ echo 'style="background-image: url('.$args['bg_image_url'.$suffix].');">'; } ?>
						<?php if( $use_overlay ) : ?>
							<div class="img-overlay" style="background-image:linear-gradient(to top right, <?php echo $args['overlay_color'.$suffix]; ?> <?php echo $args['percent'.$suffix]; ?>%, transparent ); opacity: <?php echo $args['overlay_opacity'.$suffix]; ?>;">
							</div>
						<?php endif; ?>
						<?php if( $use_popup ) : ?>
							<a class="video-btn " href="<?php echo $popup_video; ?>">
								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 100 100">
								  <defs><style>.cls-1 {clip-path: url(#clip-Icon-Video);}.cls-2 {fill: red;}.cls-3 {opacity:1;fill: #fff;}</style>
								    <clipPath id="clip-Icon-Video"><rect width="100" height="100"/></clipPath>
								  </defs><g id="Icon-Video" class="cls-1"><circle id="Ellipse_2" data-name="Ellipse 2" class="cls-2" cx="50" cy="50" r="50"/><g id="Group_170" data-name="Group 170" transform="translate(34 26.001)"><path id="Path_133" data-name="Path 133" class="cls-3" d="M38.784,21.944,4.536.728C2.04-.9,0,.3,0,3.408V44.592c0,3.1,2.04,4.312,4.536,2.68L38.784,26.056A2.926,2.926,0,0,0,40,24,2.926,2.926,0,0,0,38.784,21.944Z" transform="translate(0 0)"/></g></g>
								</svg>
							</a>
						<?php endif; ?>
						<?php if( $use_bg_video) : ?>
							<div class="video" data-vide-bg="mp4: <?php echo $args['video_file_mp4'.$suffix]; ?>"></div>
						<?php endif; ?>
					</div><!--/bg-img-->
				</div><!--img-block-wrap-->
			<?php endif; ?>
			<?php if( $args['show_content'.$suffix] ) : ?>
				<div class="media-foot-content <?php if( !$use_banner ){ echo " no-banner "; } ?>">
					<?php if( !$use_banner ) : ?>
						<div class="orange-fade-vertical left-bar"></div>
						<div class="orange-fade-horizontal top-bar"></div>
					<?php endif; ?>
					<div class="title">
						<div class="title-wrap">
<?php $use_title = 0; ?>
							<?php if( $use_title && isset( $args['subtitle'.$suffix] ) ) : ?>
								<h6><?php echo $args['subtitle'.$suffix]; ?></h6>
							<?php endif; ?>
							<?php if( $use_title && isset( $args['title'.$suffix] ) ) : ?>
								<h2><?php echo $args['title'.$suffix]; ?></h2>
							<?php endif; ?>
							<?php if(  $args['content'.$suffix]  ) : ?>
								<?php echo $args['content'.$suffix]; ?>
							<?php endif; ?>
							<?php if( $args['button'.$suffix]  ) : ?>
								<div class="button-wrapper">
									<a href="<?php echo $args['button'.$suffix]['url']; ?>" target="<?php echo $args['button'.$suffix]['target']; ?>" class="udrive-button orange-fade-horizontal ">
										<h5><?php echo $args['button'.$suffix]['title']; ?></h5>
									</a>
								</div>
							<?php endif; ?>
						</div><!--/title-wrap-->
					</div><!--/title-->
				</div><!--/meadi-foot-content-->
			<?php endif; ?>
		</div>
		<?php if(isset ($args['half_width'.$suffix]) ) : ?>

		<?php endif; ?>
	<?php } ?>
</div>
