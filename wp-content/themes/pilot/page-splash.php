<?php 
/*
Template Name: Splash
*/
get_header(); ?>
		<?php while ( have_posts() ) : the_post(); ?>
			<?php echo do_shortcode('[mason_build_blocks container=content]'); ?>
		<?php endwhile; ?>
				<?php echo do_shortcode('[mason_build_blocks container=footer is_option="1"]'); ?>

<?php get_footer(); ?>