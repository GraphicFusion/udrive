<?php
class UDLocation{
	public function __construct( $location_ref = null ){
		global $pilot;
		if( is_numeric( $location_ref ) ){
			$location = get_user_by( 'ID', $location_ref );
		}
		elseif( is_object( $location_ref ) ){
			$location = $location_ref;
		}
		if( is_object( $location ) ){
			foreach( $location as $key => $value ){
				$this->$key = $value;
			}
		}
		$this->get_location_meta();
		$this->wp_location = $location;
	}
	public function get_location_meta(){
		$this->get_address();
	}
	public function get_address(){
		$address_array = get_field('address',$this->ID);
		if(is_array($address_array)){
			$this->address = $address_array['address'];
			$this->lat = $address_array['lat'];
			$this->lng = $address_array['lng'];
		}
	}

}