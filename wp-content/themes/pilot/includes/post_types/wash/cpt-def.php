<?php
/**
 * Wash Custom Post Type
 */
add_action( 'init', 'register_wash_post_type');
function register_wash_post_type()
{ 
	register_post_type( 'wash',
		array( 'labels' => 
			array(
				'name'               => 'Washes',
				'singular_name'      => 'Wash',
				'all_items'          => 'All Washes',
				'add_new'            => 'Add New',
				'add_new_item'       => 'Add New Wash',
				'edit'               => 'Edit',
				'edit_item'          => 'Edit Wash',
				'new_item'           => 'New Wash',
				'view_item'          => 'View Wash',
				'search_items'       => 'Search Washes',
				'not_found'          => 'Nothing found in the Database.',
				'not_found_in_trash' => 'Nothing found in Trash',
				'parent_item_colon'  => ''
			),
			'description'         => 'Washes post type',
			'public'              => true,
			'publicly_queryable'  => true,
			'exclude_from_search' => false,
			'show_ui'             => true,
			'query_var'           => true,
			'menu_position'       => 10,
			'menu_icon'           => 'dashicons-cart',
			 'rewrite'	      => array( 'slug' => 'car-wash-menu', 'with_front' => false ),
			 'has_archive'      => false,
			'capability_type'     => 'page',
			'supports'            => array( 'title','author')
		)
	);
}
?>