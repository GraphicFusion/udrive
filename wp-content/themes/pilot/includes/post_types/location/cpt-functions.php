<?php
	require get_template_directory() . '/includes/post_types/location/cpt-def.php';
	require get_template_directory() . '/includes/post_types/location/cpt-acf.php';
	require get_template_directory() . '/includes/post_types/location/location.class.php';

// add lat/long to meta field when location cpt is saved - 
add_action( 'save_post', 'add_loc_lat_lng' );
function add_loc_lat_lng( $post_id ) {
	$post = get_post( $post_id );
	if( !empty ( $_POST ) && array_key_exists('post_type',$_POST)){
		if ( 'location' != $_POST['post_type'] ) {
	        return;
	    }
		if( array_key_exists( 'acf' , $_POST ) ){
			$array_key = create_key('location','address');
			if( array_key_exists( $array_key , $_POST['acf'] ) ){
				$geo = $_POST['acf'][$array_key];
			}
			if( $geo ){
				global $wpdb;
				$table_name = $wpdb->prefix . 'loc_coordinates';
				$sql_text = "INSERT INTO $table_name ( location_id, lat, lng) VALUES (%d , %f , %f) ON DUPLICATE KEY UPDATE lat = %f, lng = %f";
				$sql = $wpdb->prepare($sql_text, $_POST['post_ID'],$geo['lat'],$geo['lng'],$geo['lat'],$geo['lng']);
				$wpdb->query($sql);
			}
		}
	}
}  

function get_all_locations(){
	$locations = array();
	$args=array(
	  'post_type' => 'location',
	  'post_status' => 'publish',
	  'posts_per_page' => -1
	);
	$loc_query = new WP_Query($args);
	if( $loc_query->have_posts() ):
		foreach($loc_query->posts as $loc_post):
			$locations[] = new location($loc_post);			
		endforeach;
	endif;
	return $locations;
}
function qcc_location( $location_ref = null ){
	$location = new QCC\location( $location_ref );
	if( $location->is_admin() ){
		return new QCC\Admin( $location );
	}
	return $location;

}
add_action( 'trashed_post', 'test_function' );
function test_function( $post_id ){
	if ( 'location' != get_post_type( $post_id ) )
		return;
	
		global $wpdb;
		$table_name = $wpdb->prefix . 'loc_coordinates';
		$sql_text = "DELETE FROM $table_name where location_id = (%d )";
		$sql = $wpdb->prepare($sql_text, $post_id);
		$wpdb->query($sql);
}