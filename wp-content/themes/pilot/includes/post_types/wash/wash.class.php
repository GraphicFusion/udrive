<?php
class QCCWash{
	public function __construct( $wash_ref = null ){
		global $pilot;
		if( is_numeric( $wash_ref ) ){
			$wash = get_user_by( 'ID', $wash_ref );
		}
		elseif( is_object( $wash_ref ) ){
			$wash = $wash_ref;
		}
		if( is_object( $wash ) ){
			foreach( $wash as $key => $value ){
				$this->$key = $value;
			}
		}
		$this->get_wash_meta();
		$this->wp_wash = $wash;
	}
	public function get_wash_meta(){
		$cpt = "wash";
		$this->active = get_field($cpt.'_active',$this->ID);
		$this->image = get_field($cpt.'_image',$this->ID);
		$this->name = get_field($cpt.'_name',$this->ID);
		$this->summary = get_field($cpt.'_summary',$this->ID);
		$this->features = get_field($cpt.'_features',$this->ID);
		$this->prices = get_field($cpt.'_prices',$this->ID);
		$this->price_description = get_field($cpt.'_price_description',$this->ID);
		$this->other_info = get_field($cpt.'_other_info',$this->ID);

	}
}