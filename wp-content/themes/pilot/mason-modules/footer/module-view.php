<?php 
	/**
	 * string	$args['title']
	 * string	$args['content']
	 */
	global $args;
	$class="";
	if( $args['pull_down'] ){
		$class = "pull-down";
	}
?>
<div class="orange-fade-horizontal-reverse top-bar"></div>
<div class="footer <?php echo $class; ?>">
	<div class="footer-content interior-container">
		<div class="logo-wrapper">
			<img src="<?php echo $args['logo']['url']; ?>">
		</div>
		<div class="column-wrapper">
			<?php $cols = ['one','two','three']; foreach($cols as $col) : ?>
				<ul class="column-<?php echo $col; ?>">
					<?php if( count($args['col_'.$col])>0 ) : ?>
						<?php foreach($args['col_'.$col] as $link_arr ) : $link = $link_arr['footer_col_'.$col.'_link']; ?>
							<?php $class= ""; if(isset($link_arr['yellow']) && $link_arr['yellow']){ $class="yellow"; } ?>
							<li>
								<a href="<?php echo $link['url']; ?>" class="<?php echo $class; ?>"><?php echo $link['title']; ?></a>
							</li>
						<?php endforeach; ?>
					<?php endif; ?>
				</ul>
			<?php endforeach; ?>
		</div>
	</div>
	<div class="interior-container copyright">
			<h6>© COPYRIGHT U-DRIVE EXPRESS CAR WASH. ALL RIGHTS RESERVED.</h6>
	</div>
</div>