jQuery(document).ready(function($) {
	// Accordion script
	
	var allPanels = $('.accordion > dd').hide(),
		allPanelLabels = $('.accordion > dt');
		
	$('.accordion > dt > a').click(function() {
		if ( !$(this).parent().hasClass('active js-open') ) {
			allPanels.slideUp();
			allPanelLabels.removeClass('active js-open');
			$(this).parent().next().slideDown();
			$(this).parent().addClass('active js-open');
		}
		else {
			$(this).parent().removeClass('active js-open').next().slideUp();
		}
		return false;
	});
});
