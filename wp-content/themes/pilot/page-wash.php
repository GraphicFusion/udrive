<?php
/**
 * Template Name: Wash Menu
 *
 */
$args = array('post_type' => 'wash');
$washes = new WP_Query($args);
get_header(); ?>

	<?php if ( $washes->have_posts() ) : ?>

		<header class="page-header">
		</header>
<?php get_template_part( 'views/content', 'page' ); ?>

<div class='block-icon module wash-list' id=''>
	<div class='layout-content'>
		<div class="shadow-card interior-container">
		<?php while ( $washes->have_posts() ) : $washes->the_post(); $wash = new QCCWash($post); ?>
			<?php if($wash->active) : ?>
				<?php
					$image = "";
					if($wash->image){
						$image = '<div class="img-wrapper"><img class="content-img" src="'.$wash->image['url'].'"></div>';
					}
				?>
				<div class="icon-wrap wow animated fadeInUp">
					<?php echo $image; ?>
					<div class="icon-content ">
						<div class="title wow fadeInRight animated" data-wow-duration="2s">
							<?php if( $wash->name ) : ?>
								<h3><?php echo $wash->name; ?><span class="h4"> Wash</span></h3>
							<?php endif; ?>
							<?php if( $wash->summary ) : ?>
								<h6><?php echo $wash->summary ?></h6>
							<?php endif; ?>
							<?php if( $wash->features ) : ?>
								<ul class="udrive-list">
									<?php foreach( $wash->features as $feat) : ?>
										<li><?php echo $feat['wash_feature']; ?></li>
									<?php endforeach; ?>
								</ul>
							<?php endif; ?>
							<?php if( $wash->prices ) : ?>
								<div class="price-wrapper">
									<?php foreach( $wash->prices as $price_arr) : ?>
										<div>
											<div class="price"><?php echo $price_arr['wash_price']; ?></div>  -  <div class="price-description"><?php echo $price_arr['wash_price_description']; ?></div>
										</div>
									<?php endforeach; ?>
								</div>
							<?php endif; ?>
						</div><!--/title-->
					</div><!--/icon-content-->
				</div><!--/icon-wrap-->
			<?php endif; ?>
		<?php endwhile; ?>
		</div>
	</div>
</div>
		<?php the_posts_navigation(); ?>

	<?php else : ?>

		<?php get_template_part( 'views/content', 'none' ); ?>

	<?php endif; wp_reset_query(); ?>
<?php echo do_shortcode('[mason_build_blocks container=lower_content]'); ?>

<?php get_footer(); ?>