<?php
	function build_header_layout(){
		$args = array(
			'title' => get_sub_field('header_block_title'),
			'content' => mason_get_sub_field('header_block_content'),
			'logo' => mason_get_sub_field('header_block_logo'),
			'main_links' => mason_get_sub_field('header_block_main_nav'),
			'yellow_link' => mason_get_sub_field('header_block_yellow_link'),
			'red_link' => mason_get_sub_field('header_block_red_link'),  
		);

		return $args;
	}
?>