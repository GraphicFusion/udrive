<?php
global $pilot;
// add module layout to flexible content
$module_layout = array(
    'key' => '567c402379f4efdheaderbanner',
    'name' => 'headerbanner_block',
    'label' => 'Header Banner Block',
    'display' => 'block',
    'sub_fields' => array(
        array(
            'key' => create_key('headerbanner','image'),
            'label' => 'Background Image',
            'name' => 'headerbanner_block_image',
            'type' => 'image',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => array(
            ),
            'wrapper' => array(
                'width' => '40%',
                'class' => '',
                'id' => '',
            ),
            'return_format' => 'array',
            'preview_size' => 'thumbnail',
            'library' => 'all',
            'min_width' => '',
            'min_height' => '',
            'min_size' => '',
            'max_width' => '',
            'max_height' => '',
            'max_size' => '',
            'mime_types' => '',
        ),
        /*
        array(
            'key' => 'field_568c4ac9cderghbe48headerbanner',
            'label' => 'Apply Filter Over Background Image',
            'name' => 'headerbanner_block_modify',
            'type' => 'true_false',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '30%',
                'class' => '',
                'id' => '',
            ),
            'message' => '',
            'default_value' => 0,
        ),
        array(
            'key' => 'field_568c4afdgfbrtcde49headerbanner',
            'label' => 'Opacity',
            'name' => 'headerbanner_block_overlay_opacity',
            'type' => 'number',
            'instructions' => 'Set from 0 to 1 (for example 0.75)',
            'required' => 0,
            'conditional_logic' => array(
                array(
                    array(
                        'field' => 'field_568c4ac9cderghbe48headerbanner',
                        'operator' => '==',
                        'value' => '1',
                    ),
                ),
            ),
            'wrapper' => array(
                'width' => 35,
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'min' => 0,
            'max' => 1,
            'step' => '.01',
            'readonly' => 0,
            'disabled' => 0,
        ),
        array(
            'key' => 'field_568c4afdwef38yrg487gerv9headerbanner',
            'label' => 'Percentage of "cover"',
            'name' => 'headerbanner_block_overlay_percent',
            'type' => 'number',
            'instructions' => 'Set from 0 to 100',
            'required' => 0,
            'conditional_logic' => array(
                array(
                    array(
                        'field' => 'field_568c4ac9cderghbe48headerbanner',
                        'operator' => '==',
                        'value' => '1',
                    ),
                ),
            ),
            'wrapper' => array(
                'width' => 30,
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'min' => 0,
            'max' => 100,
            'step' => '.01',
            'readonly' => 0,
            'disabled' => 0,
        ),
        array(
            'key' => 'field_568c4rhtrtc5cf3225headerbanner',
            'label' => 'Color',
            'name' => 'headerbanner_block_overlay_color',
            'type' => 'color_picker',
            'instructions' => 'Set the color overlay for the image or video. Enter 003660 for default Mister Blue.',
            'required' => 0,
            'conditional_logic' => array(
                array(
                    array(
                        'field' => 'field_568c4ac9cderghbe48headerbanner',
                        'operator' => '==',
                        'value' => '1',
                    ),
                ),
            ),
            'wrapper' => array(
                'width' => '30',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
        ),
        array(
            'key' => 'field_5843759846gf09dheaderbanner',
            'label' => 'Left Image',
            'name' => 'headerbanner_block_left_image',
            'type' => 'image',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => array(
            ),
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'return_format' => 'array',
            'preview_size' => 'thumbnail',
            'library' => 'all',
            'min_width' => '',
            'min_height' => '',
            'min_size' => '',
            'max_width' => '',
            'max_height' => '',
            'max_size' => '',
            'mime_types' => '',
        ),
        */

    ),
    'min' => '',
    'max' => '',
);

?>